package unam.fca.examen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.FrameLayout

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        val fragmentoCategoria = CategoriaFragment()
        val fragmentTransaction = supportFragmentManager.beginTransaction()

        fragmentTransaction.apply {
            replace(R.id.fragmentoCategoria, fragmentoCategoria)
            addToBackStack(null)
            commit()
        }

    }
}