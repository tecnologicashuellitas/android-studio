package unam.fca.examen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.FrameLayout
import androidx.fragment.app.FragmentManager

class WebActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_web)

        val fragmentoWeb=WebFragment()
        val args = Bundle()
        fragmentoWeb.arguments = args
        args.putString("Url",intent.getStringExtra("Url"))
        fragmentoWeb.arguments = args
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.apply {
            replace(R.id.webView,fragmentoWeb)
            addToBackStack(null)
            commit()
        }
    }

}