package unam.fca.examen

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView

class AdaptadorTienda (private val context: Context?, private val tiendas: MutableList<Tienda>): RecyclerView.Adapter<AdaptadorTienda.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(context)
        val view = layoutInflater.inflate(R.layout.tienda_card,parent, false)
        return  ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val tienda: Tienda = tiendas[position]
        holder.txvNombre.text = tienda.nombre
        holder.ivImage.setImageResource(tienda.imagen)

        holder.itemView.setOnClickListener {
            val main = (context as Activity)
            if(main.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE){
                val fragmentoWeb=WebFragment()
                val args = Bundle()
                args.putString("Url",tienda.url)
                fragmentoWeb.arguments = args
                val fragmentManager : FragmentManager = (context as AppCompatActivity).supportFragmentManager
                val fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.apply {
                    replace(R.id.fragmentoWeb,fragmentoWeb)
                    addToBackStack(null)
                    commit()
                }
            } else {
                val intent: Intent = Intent(context, WebActivity::class.java)
                intent.putExtra("Url", tienda.url)
                main.startActivity(intent)
            }

        }
    }

    override fun getItemCount(): Int {
        return tiendas.size
    }


    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        var txvNombre : TextView
        var ivImage: ImageView

        init {
            txvNombre = itemView.findViewById(R.id.texto)
            ivImage = itemView.findViewById(R.id.imagenTienda)
        }
    }


}