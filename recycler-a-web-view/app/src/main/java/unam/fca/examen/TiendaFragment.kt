package unam.fca.examen

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MarcaFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MarcaFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var cardMarca : RecyclerView



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val vista = inflater.inflate(R.layout.fragment_tienda, container, false)
        cardMarca = vista.findViewById(R.id.cardTiendas)

        var categoria = arguments?.getString("Categoria")
        if (categoria != null) {
            iniciarCardTienda(categoria)
        }
        return vista
    }

    private fun iniciarCardTienda( categoria: String){
        cardMarca.layoutManager = LinearLayoutManager(context)
        cardMarca.setHasFixedSize(true)
        cardMarca.adapter = AdaptadorTienda(context, getMarcas().filter {
            it.categoria.equals(categoria)} as MutableList<Tienda>)
    }

    private fun getMarcas(): MutableList<Tienda> {
        val tiendas : MutableList<Tienda> = mutableListOf()
        tiendas.add(Tienda("Wings", "https://wings.com.mx/es/", "Alimentos", R.mipmap.alas))
        tiendas.add(Tienda("Burger King", "https://www.burgerking.com.mx/", "Alimentos", R.mipmap.hamburgesa))
        tiendas.add(Tienda("Subway", "https://www.subway.com/es-mx", "Alimentos", R.mipmap.ensalada))

        tiendas.add(Tienda("Sony Play", "https://www.playstation.com/es-mx/", "Electrónicos", R.mipmap.play))
        tiendas.add(Tienda("Xbox", "https://www.xbox.com/es-MX", "Electrónicos", R.mipmap.nintendo))
        tiendas.add(Tienda("Nintendo", "https://www.nintendo.com/es_LA/", "Electrónicos", R.mipmap.xbox))

        tiendas.add(Tienda("Adidas", "https://www.adidas.es/relojes", "Reloj", R.mipmap.adidas_reloj))
        tiendas.add(Tienda("Casio", "https://www.liverpool.com.mx/tienda?s=cassio", "Reloj", R.mipmap.casio))
        tiendas.add(Tienda("Tommy", "https://es.tommy.com/hombres-relojes", "Reloj", R.mipmap.tommy))

        tiendas.add(Tienda("Bershka", "https://www.bershka.com/es/", "Ropa", R.mipmap.pull))
        tiendas.add(Tienda("Marti", "https://www.marti.mx/", "Ropa", R.mipmap.marti))
        tiendas.add(Tienda("Shein", "https://www.shein.com.mx/", "Ropa", R.mipmap.shein))

        tiendas.add(Tienda("Adidas", "https://www.adidas.es/", "Tenis", R.mipmap.adidas))
        tiendas.add(Tienda("Nike", "https://www.nike.com/mx/", "Tenis", R.mipmap.nike))
        tiendas.add(Tienda("Puma", "https://www.liverpool.com.mx/tienda/puma/cat4190004", "Tenis", R.mipmap.puma))

        return tiendas
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment MarcaFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                MarcaFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}