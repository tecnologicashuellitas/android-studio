package unam.fca.examen

import android.app.Activity
import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView


class AdaptadorCategoria (private val context: Context?, private val categorias: MutableList<Categoria> ): RecyclerView.Adapter<AdaptadorCategoria.ViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(context)
        val view = layoutInflater.inflate(R.layout.categoria_card, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val categoria: Categoria = categorias[position]

        holder.txNombre.text = categoria.nombre
        holder.ivImage.setImageResource(categoria.imagen)

        holder.itemView.setOnClickListener {
            val contexto = (context as Activity)
            contexto.findViewById<FrameLayout>(R.id.fragmentoTienda).visibility = View.VISIBLE
            val fragmentoTienda = MarcaFragment()
            val args = Bundle()
            args.putString("Categoria", categoria.nombre)
            fragmentoTienda.arguments = args
            val fragmentManager: FragmentManager = (context as AppCompatActivity).supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.apply {
                replace(R.id.fragmentoTienda, fragmentoTienda)
                addToBackStack(null)
                commit()
            }

        }
    }

    override fun getItemCount(): Int {
        return categorias.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txNombre: TextView
        var ivImage: ImageView

        init {

            ivImage = itemView.findViewById(R.id.imagenCategoria)
            txNombre = itemView.findViewById(R.id.texto)


        }

    }
}





